%global vendor %{?_vendor:%{_vendor}}%{!?_vendor:openEuler}

Name: openeuler-ros
Summary: ROS package repositories
Version: 1.0.0
Release: 1
License: MulanPSL-2.0
URL: https://gitee.com/src-openeuler/openeuler-ros
Source0: openEulerROS.repo

BuildArch: noarch

%description
ROS Packages for %{vendor} repository configuration 

%install
install -D -m 644 %{SOURCE0} %{buildroot}%{_sysconfdir}/yum.repos.d/openEulerROS.repo

%files
%defattr(-,root,root)
%config(noreplace) %{_sysconfdir}/yum.repos.d/openEulerROS.repo

%changelog
* Tue Jun 13 2023 will_niutao <niutao2@huawei.com> - 1.0.0-1
- Package Init
